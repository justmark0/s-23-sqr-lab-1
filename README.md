# SQR - Lab 1
[![pipeline status](https://gitlab.com/justmark0/s-23-sqr-lab-1/badges/main/pipeline.svg)](https://gitlab.com/justmark0/s-23-sqr-lab-1/badges/main/pipeline.svg)

## Process of auto deployment
CI pipeline steps:
1. Build and push to docker hub.
2. Fetch and run new image on server.

You can access web page following this link: http://ihatedevops.justmark0.me/hello
